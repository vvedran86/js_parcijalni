// imports
import * as Classes from './entities/classes.js';

export const callAPI = function (endpoint, method) {
    new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, endpoint, true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    console.log(`XHR success status: ${xhr.status}`);
                    let response = JSON.parse(xhr.responseText);
                    readSuccess(response);
                } else {
                    readError(xhr.status);
                    console.log(`XHR response status: ${xhr.status}`);
                }
            } else {
                console.log(`XHR status text ${xhr.statusText} and ready state was stopped at ${xhr.readyState}`);
            }
        }
    });
};

export const mapJSONToObject = function (data) {
    const songDto = new Classes.SongDTO;
    data.results.forEach(element => {
        songDto.mapObject(element);
        console.log(songDto);
    });

    return songDto;
};

export const noDataAvailable = function (response) {
    return [
        `<div class="alert alert-danger" role="alert">
            Nažalost, tekst koji ste pretraživali nije rezultirao niti jednim rezultatom. Pokušajte s drugačijim izrazom ponovo.
            <p>
                ${response}
            </p>
        </div>`
    ];
};

export const writeHTML = (data) => {
    // glavni DIV u koji stavljamo elemente
    const divArtistList = document.getElementById('artistList');

    if(divArtistList.firstChild) {
        divArtistList.innerHTML = '';
    }

    data.results.forEach(element => {
        // kreiramo elemente u koje cemo stavljati sadrzaj
        const divMD4 = createNewElement('div');
        // const divCard = createNewElement('div');
        // const cardImg = createNewElement('img');
        // const divCardBody = createNewElement('div');
        // const cardBodyH5 = createNewElement('h5');
        // const cardBodyP = createNewElement('p');
        // const cardBodyA = createNewElement('a');

        // dodajemo klasu elementu
        addClassNameToElement(divMD4, 'col-md-4');
        // addClassNameToElement(divCard, 'card');
        // divCard.style.width = "18rem"; //custom tag for width
        // addClassNameToElement(cardImg, 'card-img-top');
        // addClassNameToElement(divCardBody, 'card-body');
        // addClassNameToElement(cardBodyH5, 'card-title');
        // addClassNameToElement(cardBodyP, 'card-text');
        // addClassNameToElement(cardBodyA, 'btn-primary');

        // dodaj vrijednosti u elemente
        // cardImg.src = element.artworkUrl100;
        // addTextContentToElement(cardBodyH5, element.artistName);
        // addTextContentToElement(cardBodyP, element.collectionName);
        // cardBodyA.href = element.previewUrl;

        // zapisi element u DOM
        // appendChildToElement(divArtistList, divCard);
        // appendChildToElement(divCard, cardImg);
        // appendChildToElement(divCard, divCardBody);
        // appendChildToElement(divCardBody, cardBodyH5);
        // appendChildToElement(divCardBody, cardBodyP);
        // appendChildToElement(divCardBody, cardBodyA);
        let artistInfo = generateHTMLElement(element.artworkUrl100, element.artistName, element.previewUrl, element.collectionName, element.trackName);
        divMD4.innerHTML = artistInfo;

        appendChildToElement(divArtistList, divMD4);
    });
};

let generateHTMLElement = function(cardImgSrc, artistName, previewUrl, collectionName, trackName) {
    return [
    `<div class="card mb-4 shadow-sm">
        <img src="${cardImgSrc}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${artistName}</h5>
                <p class="card-text">${collectionName}</p>
                <p class="card-text">${trackName}</p>
                <div class="d-flex justify-content-between align-items-center">
                <a href="${previewUrl}" class="btn btn-primary">Poslušaj pjesmu</a>
            </div>
    </div>`
    ];
};

const readError = function (response) {
    noDataAvailable(response);
};

const readSuccess = function (response) {
    writeHTML(response);
};

const createNewElement = function (element) {
    return document.createElement(element);
};

const addClassNameToElement = function(element, className) {
    element.classList.add(className);
};

const insertIntoDOM = function(domElement, newElement) {
    document.body.insertAfter(domElement, newElement);
};

const addTextContentToElement = function(element, text) {
    let textNode = document.createTextNode(text);
    element.appendChild(textNode);
};

const appendChildToElement = function(mainElement, childElement) {
    mainElement.appendChild(childElement);
};

export const constructAPIUrl = function (searchText, entitySelected, limitSelected, url) {
    let cleanString = searchText.replace(' ', '%20');
    const apiParams = `?term=${cleanString}&entity=${entitySelected}&limit=${limitSelected}`;
    const endpoint = `${url}${apiParams}`;
    return endpoint;
};

export const getEntityTypeSelected = function (ddlList) {
    let entitySelected = ddlList.options[ddlList.selectedIndex].value;
    return entitySelected;
};

export const getUserInputTextValue = function (userInput) {
    return userInput.value;
};