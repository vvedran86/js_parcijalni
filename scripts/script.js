// imports
import * as Variables from './variables.js';
import * as Classes from './entities/classes.js';
import * as Helper from './helper.js';

const buttonSubmit = document.getElementById('submitQuery');
const ddlList = document.getElementById("entityList");
const limitList = document.getElementById("limitList");
const userInput = document.getElementById('userInput');

const makeAPICall = function() {
    // uzmi tekst koji je user upisao
    let userText = Helper.getUserInputTextValue(userInput);

    // provjera jel nesto upisano
    if(!userText) {
        alert("Molim, unesite tekst koji želite pretražiti");
        return;
    }

    // uzmi tip pretrage koju je korisnik odabrao
    let entitySelected = Helper.getEntityTypeSelected(ddlList);

    // provjera jel nesto odabrano
    if(entitySelected === "0") {
        alert("Niste odabrali tip pretraživanja iz padajuće liste");
        return;
    }

    // uzmi limit koji je korisnik odabrao
    let limitSelected = Helper.getEntityTypeSelected(limitList);

    // konstruiraj kompletan URL na temelju inputa korisnika
    let endpoint = Helper.constructAPIUrl(userText, entitySelected, limitSelected, Variables.url);

    // pozovi Helper koji ce vratiti JSON promise ili error ako bude greska
    Helper.callAPI(endpoint, Variables.method);
};

buttonSubmit.addEventListener('click', makeAPICall);
document.addEventListener('keypress', function(e) {
    if(e.key === 'Enter') {
        makeAPICall();
    }
});